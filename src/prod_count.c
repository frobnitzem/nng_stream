#include "prod_count.h"

int prod_count_ctor(struct prod_count *prod,
                    const int expect) {
    int rc;

    if(expect < 1 || expect >= (1<<16)) {
        return NNG_ENOTSUP;
    }
    if( (rc = nng_mtx_alloc(&prod->mtx)) != 0) {
        return rc;
    }
    prod->expect = expect;
    return 0;
}

void prod_count_dtor(struct prod_count *prod) {
    nng_mtx_free(prod->mtx);
}

bool prod_count_done(struct prod_count *prod) {
    return prod->seen >= prod->expect
                && prod->connected == 0;
}

void prod_count_incr(struct prod_count *prod) {
    prod->seen++;
    prod->connected++;
}

void prod_count_decr(struct prod_count *prod) {
    prod->connected--;
}
