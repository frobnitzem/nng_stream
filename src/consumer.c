#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <nng/nng.h>
#include <nng/protocol/pipeline0/pull.h>
#include <nng/supplemental/util/platform.h>

pid_t my_pid;
char done = 0;

void
fatal(const char *func, int rv)
{
	fprintf(stderr, "%s: %s\n", func, nng_strerror(rv));
	exit(1);
}

void
pipe_cb(nng_pipe pipe, nng_pipe_ev event, void *arg)
{
    done = 1;
}

int
consumer(const char *url)
{
	nng_socket   sock;
	int          rv;
    nng_time     start, end;
    int          nmsg  = 0;
    size_t       nbyte = 0;
    nng_duration timeout = 100;
    unsigned     msec;

	if( (rv = nng_pull0_open(&sock)) != 0) {
		fatal("nng_pull0_open", rv);
	}
    if( (rv = nng_socket_set_ms(sock, NNG_OPT_RECVTIMEO,
                                timeout)) != 0) {
        fatal("nng_socket_set_ms", rv);
    }
    if( (rv = nng_pipe_notify(sock, NNG_PIPE_EV_REM_POST,
                              pipe_cb, NULL)) != 0 ) {
		fatal("nng_pipe_notify", rv);
	}

	if ((rv = nng_dial(sock, url, NULL, 0)) != 0) {
		fatal("nng_dial", rv);
	}
    start = nng_clock();
	while(!done) {
		char *buf = NULL;
		size_t sz;
		if ((rv = nng_recv(sock, &buf, &sz, NNG_FLAG_ALLOC)) != 0) {
            if(rv == NNG_ETIMEDOUT) {
                if(!done) {
                    printf("%d consumer: waiting for data.\n", my_pid);
                }
                continue;
            }
            if(rv == NNG_ECLOSED) {
                printf("%d consumer: Pipe closed.\n", my_pid);
                break;
            }
			fatal("nng_recv", rv);
		}
        nmsg++;
        nbyte += sz;
		nng_free(buf, sz);
	}
    end = nng_clock();
    msec = end-start;
    printf("%d Received %d messages in %u milliseconds: %f MB/s\n",
           my_pid, nmsg, msec, nbyte*1000.0/(msec*1024.0*1024.0));
}

int
main(int argc, char **argv)
{
	int rc;
    my_pid = getpid();

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <url>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	rc = consumer(argv[1]);
    nng_fini();
	exit(rc == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
