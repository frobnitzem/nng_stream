#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <nng/nng.h>
#include <nng/protocol/pipeline0/push.h>
#include <nng/supplemental/util/platform.h>

#define MSG_SIZE 128*128*8

pid_t my_pid;

void
fatal(const char *func, int rv)
{
	fprintf(stderr, "%s: %s\n", func, nng_strerror(rv));
	exit(1);
}

int
producer(const char *url, int nmsg)
{
	nng_socket sock;
	int rv;
    nng_time start, end;
    char buf[MSG_SIZE];
    unsigned msec;

	if ((rv = nng_push0_open(&sock)) != 0) {
		fatal("nng_push0_open", rv);
	}
	if ((rv = nng_dial(sock, url, NULL, 0)) != 0) {
		fatal("nng_dial", rv);
	}
    start = nng_clock();
	for (int i=0; i<nmsg; i++) {
		if ((rv = nng_send(sock, buf, MSG_SIZE, 0)) != 0) {
			fatal("nng_send", rv);
		}
	}
    end = nng_clock();
    msec = end-start;
    printf("%d Sent %d messages in %u milliseconds: %f MB/s\n",
           my_pid, nmsg, msec, 1000.0*nmsg*MSG_SIZE/(msec*1024.0*1024.0));
}

int
main(int argc, char **argv)
{
	int rc;

    my_pid = getpid();
	if (argc != 3) {
		fprintf(stderr, "Usage: %s <url> <nmsg>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	rc = producer(argv[1], atoi(argv[2]));
    nng_fini();
	exit(rc == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
