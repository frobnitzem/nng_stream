#include <pthread.h>
#include <zmq.h>
#include "cache.h"

typedef void * zmq_socket_t;
typedef void * zmq_context_t;
struct work {
    // pusher enters waiting state if queue is empty
    // puller enters waiting state if queue is full
    enum RunState state; // INIT,WAIT,ACTIVE
    nng_aio *aio;
    nng_msg *msg;
    nng_socket sock;
    nng_duration sleep;
    zmq_socket_t zmq_sock;
};


/* make global to have one activity wake up the other
 */
struct work pull_state;
struct work push_state;
// These two are not owned by the work structure:
RingBuf msg_buf;
struct prod_count prodc;

struct program_state prog_state;
pid_t my_pid;
int verbose;
bool dial_recv;

void
puller_cb(void *arg)
{
    /* Pull data from all producers who connect.
     *
     * It is the only activity moving msg_buf head,
     * which always goes forward.
     */
    struct work *work = arg;
    RingBuf     *buf = &msg_buf;
    int          rv;
    int          end;

    switch (work->state) {
    case INIT:
        work->state = ACTIVE;
        work->sleep = SLEEP_MIN;
        nng_recv_aio(work->sock, work->aio);
        break;
    case WAIT:
        if(is_full(buf)) { // still full
            work->sleep *= SLEEP_MULTIPLE;
            if(work->sleep > SLEEP_MAX) {
                work->sleep = SLEEP_MAX;
            }
            nng_sleep_aio(work->sleep, work->aio);
        } else {
            work->state = ACTIVE;
            log_debug("%d PULL: entering receiving state");
            nng_recv_aio(work->sock, work->aio);
        }
        break;
    case ACTIVE:
        if( (rv = nng_aio_result(work->aio)) != 0) {
            if(rv == NNG_EAGAIN) {
                nng_mtx_lock(prog_state.mtx);
                // Step 3 of the shutdown sequence
                // PULL thread woken in DRAIN state.
                end = prog_state.state == DRAIN;
                nng_mtx_unlock(prog_state.mtx);
                if(end)
                    goto done;
                log_error("%d PULL woken during non-shutdown");
            /*} else if(rv == NNG_ECLOSED) {
        work->state = WAIT;
        log_debug("%d PULL: No senders, entering waiting state");
        work->sleep = SLEEP_MAX;
        nng_sleep_aio(work->sleep, work->aio);*/
            } else {
                fatal("nng_recv_aio", rv);
            }
        }

        RingBuf_put(buf, nng_aio_get_msg(work->aio));
        // nng_aio_abort(push_state.aio, NNG_EAGAIN);

        if(is_full(buf)) { // buffer is full
            work->state = WAIT;
            log_debug("%d PULL: Full buffer, entering waiting state");
            work->sleep = SLEEP_MIN;
            nng_sleep_aio(work->sleep, work->aio);
        } else {
            nng_recv_aio(work->sock, work->aio);
        }
        break;
    default:
        fatal("bad state!", NNG_ESTATE);
        break;
    }
    return;

done:
    log_info("%d PULL thread exiting.");

    // Initiate step 4 of shutdown sequence.
    nng_mtx_lock(prog_state.mtx);
    prog_state.state = DONE;
    nng_cv_wake(prog_state.cond);
    nng_mtx_unlock(prog_state.mtx);
}

/* Try to push a message.  If there is no message,
 * enter a sleep-loop until a message arrives or we are
 * out of messages.
 *
 * Returns "true" if we're done, and "false" otherwise.
 */
bool
send_reply(struct work *work)
{
    RingBuf     *buf  = &msg_buf;

    while(true) {
        // 1. Try to send a message.
        if(RingBuf_get(buf, &work->msg)) {
            if(work->state == WAIT) {
                work->state = ACTIVE;
                log_debug("%d PUSH: entering sending state");
            }
            size_t len = nng_msg_len(work->msg);

            /* Copy the message to zmq_msg_t */
            zmq_msg_t msg;
            int rc = zmq_msg_init_size(&msg, len);
            if(rc != 0) {
                fatal("zmq_msg_init_size", rc);
            }
            memcpy(zmq_msg_data(&msg), nng_msg_body(work->msg), len);
            nng_msg_free(work->msg); // release the msg buffer

            /* Send the message to the socket */
            rc = zmq_msg_send(&msg, work->zmq_sock, 0);
            if(rc != len) {
                fatal("zmq_msg_send", rc);
            }
            //nng_send_aio(work->sock, work->aio);
            return false;
        }

        // 2. Check if we are done.
        nng_mtx_lock(prodc.mtx);
        bool done = prod_count_done(&prodc);
        nng_mtx_unlock(prodc.mtx);
        if(done) {
            // send 0-length sentinel
            zmq_send(work->zmq_sock, "", 0, 0);
            return true;
        }

        // 3. If not, transition to WAIT and set sleep
        //    backoff interval.
        if(work->state == WAIT) {
            work->sleep *= SLEEP_MULTIPLE;
            if(work->sleep > SLEEP_MAX) {
                work->sleep = SLEEP_MAX;
            }
        } else {
            work->state = WAIT;
            work->sleep = SLEEP_MIN;
            log_debug("%d PUSH: Empty buffer, entering waiting state");
        }
        usleep(work->sleep*1000);
    }
}

/*** Send sentinal values
 * as long as requests come in before the timeout ***/
void
send_sentinal(zmq_socket_t socket, int timeout)
{
    char recv[8];
    zmq_pollitem_t items[1];
    items[0].socket = socket;
    items[0].events = ZMQ_POLLIN;
    while(true) {
        int rc = zmq_poll(items, 1, timeout);
        if(rc < 0) {
            fatal("zmq_poll", rc);
        }
        if(rc == 0) break;
        int nbytes = zmq_recv(socket, recv, sizeof(recv), 0);
        if(nbytes == -1) {
            fatal("zmq_recv", zmq_errno());
        }
        zmq_send(socket, "", 0, 0);
    }
}

/*** Pusher Thread ***/

void *
pusher_thread(void *arg)
{
    /* Push data via ZMQ
     *
     * It is the only activity moving msg_buf tail,
     * which always goes forward.
     */
    struct work *work = arg;
    RingBuf     *buf  = &msg_buf;
    int          rc;
    char         recv[8];

    work->state = ACTIVE;
    while(true) {
        int nbytes = zmq_recv(work->zmq_sock, recv, sizeof(recv), 0);
        if(nbytes == -1) {
            fatal("zmq_recv", zmq_errno());
        }
        if(send_reply(work)) {
            break;
        }
    }
    send_sentinal(work->zmq_sock, 100);

    log_info("%d PUSH thread exiting.");
    // Step 2 of the shutdown sequence - no messages.
    // We know prog_state.state == DRAIN and
    // the pull thread is waiting for input
    // with no producers.
    nng_aio_abort(pull_state.aio, NNG_EAGAIN);
    return NULL;
}

/* This callback is triggered on producer connect
 * and disconnect events.
 */
void
producer_pipe_cb(nng_pipe pipe, nng_pipe_ev event, void *arg)
{
    struct prod_count *prod = arg;

    log_info("%d Client pipe event: %d", event);
    nng_mtx_lock(prod->mtx);
    switch(event) {
    case NNG_PIPE_EV_ADD_PRE: // A new producer is about to connect.
        if(prod_count_done(prod)) {
            log_info("%d Rejecting new producer connection.\n");
            nng_pipe_close(pipe);
        } else {
            prod_count_incr(prod);
        }
        break;
    case NNG_PIPE_EV_REM_POST: // A producer has just disconnected.
        prod_count_decr(prod);
        if(prod_count_done(prod)) {
            // Step 1 of shutdown sequence -- last producer disconnects.
            nng_mtx_lock(prog_state.mtx);
            prog_state.state = DRAIN;
            nng_mtx_unlock(prog_state.mtx);

            // We can't know what state the PUSH thread is in,
            // so just wake it.
            //nng_aio_abort(push_state.aio, NNG_EAGAIN);
        }
        break;
    default:
        log_error("%d Unknown pipe event.");
        break;
    }
    nng_mtx_unlock(prod->mtx);
}

void
work_ctor(struct work *w, nng_socket sock, void(*cb)(void *))
{
    int rv;

    if ((rv = nng_aio_alloc(&w->aio, cb, w)) != 0) {
        fatal("nng_aio_alloc", rv);
    }
    w->sock = sock;
    w->state = INIT;
}

void
work_dtor(struct work *w)
{
    nng_aio_free(w->aio);
}


int
client_start(const char *url)
{
    nng_socket   sock = NNG_SOCKET_INITIALIZER;
    int          rv;

    rv = nng_pull0_open(&sock);
    if (rv != 0) {
        fatal("nng_pull0_open", rv);
    }
    work_ctor(&pull_state, sock, puller_cb);

    if( (rv = nng_pipe_notify(sock, NNG_PIPE_EV_ADD_PRE,
                              producer_pipe_cb, &prodc)) != 0 ||
        (rv = nng_pipe_notify(sock, NNG_PIPE_EV_REM_POST,
                              producer_pipe_cb, &prodc)) != 0 ) {
        fatal("nng_pipe_notify", rv);
    }
    if(dial_recv) {
        if( (rv = nng_dial(sock, url, NULL, 0)) != 0) {
            fatal("nng_dial", rv);
        }
    } else {
        if( (rv = nng_listen(sock, url, NULL, 0)) != 0) {
            fatal("nng_listen", rv);
        }
    }

    puller_cb(&pull_state); // run INIT state
    return 0;
}

int
server_start(const char *url,
             pthread_t *server,
             zmq_context_t *context)
{
    //work_ctor(&push_state, NULL, no_func);
    push_state.zmq_sock = zmq_socket(context, ZMQ_REP);
    int rc = zmq_bind(push_state.zmq_sock, url);
    if(rc != 0) {
        fatal("zmq_bind", zmq_errno());
    }
    pthread_create(server, NULL, pusher_thread, (void *)&push_state);
    return 0;
}

void
prog_init() {
    int rv;
    prog_state.state = INIT;
    if( (rv = nng_mtx_alloc(&prog_state.mtx)) != 0) {
        fatal("nng_mtx_alloc", rv);
    }
    if( (rv = nng_cv_alloc(&prog_state.cond, prog_state.mtx)) != 0) {
        fatal("nng_cv_alloc", rv);
    }
}

void
prog_fini() {
    //prog_state.state should be DONE;
    nng_cv_free(prog_state.cond);
    nng_mtx_free(prog_state.mtx);
}

int
main(int argc, char **argv)
{
    int rv;
    static const int BUFFER_LEN = 1024;
    RingBufElement buffer[BUFFER_LEN];
    pthread_t server;
    zmq_context_t context = zmq_ctx_new();

    my_pid = getpid();

    verbose = 0;
    dial_recv = false;
    while(argc > 1) {
        if (argv[1][0] == '-' && argv[1][1] == 'v') {
            // Set verbosity level
            verbose = strlen(argv[1])-1;
            argv[1] = argv[0];
            argv++;
            argc--;
        } else if (argv[1][0] == '-' && argv[1][1] == 'd') {
            dial_recv = true;
            argv[1] = argv[0];
            argv++;
            argc--;
        } else {
            break;
        }
    }
    if (argc != 3 && argc != 4) {
        fprintf(stderr, "Usage: %s [-vd] <listen url> <push url> [producers]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // construct global data structures
    rv = prod_count_ctor(&prodc, argc > 3 ? atoi(argv[3]) : 1);
    if(rv != 0) {
        fatal("prod_count_ctor", rv);
        exit(EXIT_FAILURE);
    }
    RingBuf_ctor(&msg_buf, buffer, BUFFER_LEN);
    prog_init();

    // Lock this here (as opposed to before the main while-loop) so
    // that potential connect/disconnects don't cause weird transitions.
    nng_mtx_lock(prog_state.mtx);

    // create client and server states
    if( (rv = server_start(argv[2], &server, context)) != 0) {
        log_error("%d Unable to start server");
        exit(EXIT_FAILURE);
    }
    if( (rv = client_start(argv[1])) != 0) {
        log_error("%d Unable to start client");
        exit(EXIT_FAILURE);
    }

    prog_state.state = ACTIVE;
    while (prog_state.state != DONE) {
        const int seconds = 1000;
        nng_time until = nng_clock() + 10*seconds;
        if (nng_cv_until(prog_state.cond, until) == NNG_ETIMEDOUT) {
            log_debug("%d Main loop checking in.");
        }
    }
    void *ret;
    // Shutdown sequence complete.
    // The program should now be in DONE state.
    nng_mtx_unlock(prog_state.mtx);

    log_info("%d Closing push socket.");
    pthread_join(server, &ret);
    zmq_close(push_state.zmq_sock);

done:
    work_dtor(&pull_state);
    //work_dtor(&push_state);
    prog_fini();
    prod_count_dtor(&prodc);
    zmq_ctx_destroy(context);
    nng_fini();
    return rv ? EXIT_FAILURE : EXIT_SUCCESS;
}
