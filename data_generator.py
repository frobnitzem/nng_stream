from collections.abc import Iterable
from typing import Tuple
#import time

import numpy as np

def image_generator(dim : Tuple, nmsg : int) -> Iterable[np.ndarray]:
    for i in range(nmsg):
        # Sleep for a random number of seconds
        #time_to_sleep = random.randint(1, 11) / 1000.0
        #time.sleep(time_to_sleep)
        yield np.random.random(dim)

def test_generator():
    gen = image_generator( (2,3), 2 )
    for x in gen:
        assert x.shape == (2,3)
        print( x )
