# nanomsg buffer

This is a simple nanomsg-based message buffer.

Connectivity looks like this:

```mermaid
flowchart LR
    subgraph caches
      direction TB
      C1[(Cache1)]
      C2[(Cache2)]
    end
    Control ---|Bus| caches

    S1[/Source1/] -->|Push/Pull| C1
    S2[/Source2/] -->|Push/Pull| C2
    S3[/Source3/] -->|Push/Pull| C2

    C1 -->|Push/Pull| Client1
    C1 --> Client2
    C2 -->|Push/Pull| Client2
    C2 --> Client4
```

Every source pushes its data to a unique cache server.
The cache servers push their data to connected compute nodes.
Compute nodes typically pull from just one cache.

Normal termination occurs when all producers have
disconnected, and the cache sends the last available
message.  To prevent an early disconnection from
shutting down the cache, the "Active" state
tracks whether all expected producers have
connected at least once.  These expected
producers must be communicated in the Control's
Start message.

<!--
Once in the Drain state, the cache closes
producer/consumer socket listeners, and opens
a Pull connection by dialing all other caches.
This helps load-balance.
When the last message is sent from this final
round, the cache returns to its initial state.
-->

Caches push data to clients in FIFO order.
<!--
When a cache is about to send a client sentinel
value, it closes its client connection, then
opens Pull connections to all other caches.
After receiving sentinel values from each cache, it
pushes sentinel values until the Bus sends a "stop" message.
-->

```mermaid
stateDiagram
    Init   --> Active: C/Start
    Active --> Drain: C/Stop or LastSend
    Drain  --> Init: LastSend
%% Alternatively, we could have the Active state
%% cycle between pulling data from Producers and other Caches
%% However, this requires more tuning to get right.
```

<!--
All clients and servers send and receive control
messages to/from a single server.  The server
serves clients via 

The server keeps a count of clients (producers)
and initiates the shutdown process.
Both Caches and Clients register with the Bus on startup, and send
a "stop" message to the Bus on shutdown.  Once all clients
have terminated, the Bus sends one "stop" message to each Cache.
-->


## Running

(using the rc shell)
```
./cache tcp://127.0.0.1:5551 tcp://127.0.0.1:5561 & C1=$apid
./cache tcp://127.0.0.1:5552 tcp://127.0.0.1:5562 & C2=$apid

./producer tcp://127.0.0.1:5551 2048 & P1=$apid
./producer tcp://127.0.0.1:5552 2048 & P2=$apid
./producer tcp://127.0.0.1:5552 2048 & P3=$apid

./consumer tcp://127.0.0.1:5561 & S1=$apid
./consumer tcp://127.0.0.1:5561 & S2=$apid
./consumer tcp://127.0.0.1:5562 & S3=$apid
./consumer tcp://127.0.0.1:5562 & S4=$apid

for(C in $S1 $S2 $S3 $S4) { wait $C }
kill $C1 $C2
```

Output for 1 producer, 1 cache, 1 consumer:
```
nng_stream% rc ./run1.rc 
Client pipe event: 1
PUSH: woken from sleep
PUSH: entering sending state
PUSH: Empty buffer, entering waiting state
PUSH: woken from sleep
PUSH: entering sending state
PUSH: Empty buffer, entering waiting state
...
PUSH: entering sending state
Sent 10000 messages in 454 milliseconds: 2753.303965 MB/s
Client pipe event: 2
PUSH: Empty buffer, entering waiting state
Received 10000 messages in 556 milliseconds: 2248.201439 MB/s
```

Output for 3 producer, 2 cache, 4 consumer:
```
nng_stream% rc ./run.rc 
Client pipe event: 1
Client pipe event: 1
Client pipe event: 1
PUSH: woken from sleep
PUSH: entering sending state
PUSH: woken from sleep
PUSH: entering sending state
PUSH: Empty buffer, entering waiting state
...
PUSH: entering sending state
Sent 3000 messages in 248 milliseconds: 1512.096774 MB/s
Client pipe event: 2
PUSH: Empty buffer, entering waiting state
PULL: entering receiving state
PUSH: woken from sleep
PUSH: entering sending state
Received 1510 messages in 352 milliseconds: 536.221591 MB/s
Received 1490 messages in 353 milliseconds: 527.620397 MB/s
Sent 3000 messages in 441 milliseconds: 850.340136 MB/s
Client pipe event: 2
Sent 3000 messages in 444 milliseconds: 844.594595 MB/s
Client pipe event: 2
PUSH: Empty buffer, entering waiting state
Received 2913 messages in 569 milliseconds: 639.938489 MB/s
Received 3087 messages in 569 milliseconds: 678.163445 MB/s
```
Aggregate throughput = 3207 MB/s.


Python producer/consumer example:
```
% python3 push.py 1 128 1024
Sent 1024 messages in 0.5711843967437744 seconds: 224.0957573941206 MB/sec.
result 1024
Received 1024 messages in 0.6628665924072266 seconds: 193.1006954735234 MB/sec.
result 1024
```

# Notes

The NNG library runs callbacks on separate threads,
so it is possible for multiple callbacks
to be executing in parallel.
This was determined by experimental testing, toggling
a global `cb_running` state on entry/exit of each
callback.

# Roadmap

x. Add outer loop (start/stop client/server listeners).

x. Add cache transition to Drain state.

x. Add pipe notification to client connections
   ~~maintain list of client connections~~
   - prevent new client connections when in DRAIN state
   - close consumer pipes on exit ~> triggers notifications

4. Trigger outer loop transitions from Control connection

5. Test outer [Init -> Active -> Drain] loop for cache.

6. Have cache Dial new Pull connections on Active -> Drain trs.
