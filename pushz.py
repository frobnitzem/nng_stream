#!/usr/bin/env python3
# Note a more "raw" socket format can be gleaned
# from https://stackoverflow.com/questions/65822261/making-pynng-and-socket-talk-to-each-other
#
# Which references:
# - https://staysail.github.io/nng_presentation/nng_presentation.html
# - https://github.com/nanomsg/nanomsg/blob/master/rfc/sp-tcp-mapping-01.txt

import time

from pynng import Push0

from data_generator import image_generator
from serialz import serialize, call_send

send_opts = {
          "send_buffer_size": 32, # 32 
        }

def pusher(addr, dim, n):
    start = time.time()
    nbyte = 0
    with Push0(dial=addr, **send_opts) as push:
        for x in image_generator((dim,dim), n):
            data = {"image": x}
            b = serialize(data)
            nbyte += len(b)
            push.send(b)
            #nbyte += call_send(push.send, data)
    t = time.time() - start
    print(f"Sent {n} messages in {t} seconds: {nbyte/t/1024**2} MB/sec.")
    return n

def main(argv):
    assert len(argv) == 3, f"Usage: {argv[0]} <url> <nmsg>"
    dim   = 128
    addr = argv[1]
    nmsg  = int(argv[2])
    pusher(addr, dim, nmsg)

if __name__=="__main__":
    import sys
    main(sys.argv)
