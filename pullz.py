#!/usr/bin/env python3
import time

from pynng import Pull0, Timeout

from serial import deserialize

recv_opts = {
          "recv_max_size": 1024*1024, # 1 MB per-packet
          "recv_timeout": 100, # 100 ms timeout
        }

def puller(addr):
    done = False

    def show_open(pipe):
        print("pullz: pipe opened")

    def show_close(pipe):
        nonlocal done
        print("pullz: pipe closed")
        done = True

    start = time.time()
    n = 0
    nbyte = 0
    with Pull0(dial=addr, **recv_opts) as pull:
        pull.add_post_pipe_connect_cb(show_open)
        pull.add_post_pipe_remove_cb(show_close)
        while not done:
            try:
                b = pull.recv()
            except Timeout:
                if not done:
                    print("pullz: waiting for data")
                continue
            n += 1
            nbyte += len(b)
            y = deserialize(b)
            assert "image" in y
    t = time.time() - start
    print(f"Received {n} messages in {t} seconds: {nbyte/t/1024**2} MB/sec.")
    return n

def main(argv):
    assert len(argv) == 2, f"Usage: {argv[0]} <addr>"
    addr = argv[1]
    puller(addr)

if __name__=="__main__":
    import sys
    main(sys.argv)
