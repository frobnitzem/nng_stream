from typing import Dict
import io
import ctypes

import numpy as np

def call_send(send, x : Dict[str,np.ndarray], compress=False) -> int:
    with io.BytesIO() as f:
        # see also savez_compressed
        if compress:
            np.savez_compressed(f, **x)
        else:
            np.savez(f, **x)

        # https://stackoverflow.com/questions/67400792/python3-memoryview-object-to-c-function-char-parameter
        n = f.getbuffer().nbytes
        send( (ctypes.c_ubyte*n).from_buffer( f.getbuffer() ) )
    return n

# Messages will be serialized as npy file format
def serialize(x : Dict[str,np.ndarray], compress=False) -> bytes:
    with io.BytesIO() as f:
        # see also savez_compressed
        if compress:
            np.savez_compressed(f, **x)
        else:
            np.savez(f, **x)
        #b = f.getbuffer()
        b = f.getvalue()
    return b

def deserialize(b : bytes) -> Dict[str,np.ndarray]:
    with io.BytesIO(b) as f:
        npz = np.load(f, allow_pickle=False) # np.lib.npyio.NpzFile
        return dict( (k,npz[k]) for k in npz.files )

def test_serialize():
    arr = {}
    shapes =  [(10,), (32,32), (1,1024,1024)]
    for n, shape in enumerate(shapes):
        x = np.random.random( shape )
        arr[str(n)] = x

    for args in [{}, {"compress": True}]:
        b = serialize(arr, **args)
        print(args, len(b), sum(z.nbytes for z in arr.values()))
        y = deserialize(b)
        for n, shape in enumerate(shapes):
            k = str(n)
            assert y[k].shape == shape
            assert np.allclose(y[k], arr[k])
