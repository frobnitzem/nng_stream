import time
import concurrent.futures
import math

import numpy as np
from pynng import Push0, Pull0, Timeout

from data_generator import image_generator
from serial import serialize, deserialize

send_opts = {
          "send_buffer_size": 32, # 32 
        }
recv_opts = {
          "recv_max_size": 1024*1024, # 1 MB per-packet
          "recv_timeout": 100, # 100 ms timeout
        }

def pusher(addr, dim, n):
    start = time.time()
    nbyte = 0
    with Push0(listen=addr, **send_opts) as push:
        for x in image_generator((dim,dim), n):
            nbyte += x.nbytes
            push.send(serialize(x))
    t = time.time() - start
    print(f"Sent {n} messages in {t} seconds: {nbyte/t/1024**2} MB/sec.")
    return n

def puller(addr):
    time.sleep(0.01) # give pusher some time to bind()
    start = time.time()
    n = 0
    nbyte = 0
    try:
        with Pull0(dial=addr, **recv_opts) as pull:
            while True:
                y = deserialize(pull.recv())
                n += 1
                nbyte += y.nbytes
    except Timeout:
        pass
    t = time.time() - start
    print(f"Received {n} messages in {t} seconds: {nbyte/t/1024**2} MB/sec.")
    return n

def run(addr, nsend, nrecv, dim, nmsg):
    with concurrent.futures.ProcessPoolExecutor(max_workers=nsend+nrecv) \
            as executor:
        tasks = []
        for i in range(nsend):
            t = executor.submit(pusher, addr, dim, nmsg)
            tasks.append(t)
        for i in range(nrecv):
            t = executor.submit(puller, addr)
            tasks.append(t)
        #for t in concurrent.futures.as_completed(tasks):
        for t in tasks:
            print("result", t.result())

def main(argv):
    assert len(argv) == 4, f"Usage: {argv[0]} <nrecv> <dim> <nmsg>"
    addr = "tcp://127.0.0.1:31313"
    nsend = 1
    nrecv = int(argv[1])
    dim   = int(argv[2])
    nmsg  = int(argv[3])
    run(addr, nsend, nrecv, dim, nmsg)

if __name__=="__main__":
    import sys
    main(sys.argv)
