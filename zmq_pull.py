import zmq
import sys
import time

def main(argv):
    url = argv[1]
    context = zmq.Context()
    print(f"Connecting to {url}")
    socket = context.socket(zmq.REQ)
    socket.connect(url)

    t0 = time.time()
    nmsg = 0
    nbyte = 0
    #  Do requests until receiving a zero-size response
    request = 0
    while True:
        request += 1
        if request < 10:
            print("Sending request ", request,"...")
        socket.send(b"Hello")
        #  Get the reply.
        message = socket.recv()
        nmsg += 1
        nbyte += len(message)
        if request < 10:
            #print("Received reply ", request, "[", message, "]")
            print(f"Received reply {request} len = {len(message)}")
        if len(message) == 0:
            break
    t1 = time.time() - t0
    nbyte /= 1024**2
    print(f"Received {nmsg} messages in {t1} seconds: {nbyte/t1} MB/sec")

if __name__=="__main__":
    main(sys.argv)
