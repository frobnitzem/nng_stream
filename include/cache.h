#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h> // getpid(), usleep()

#include <nng/nng.h>
#include <nng/protocol/pipeline0/pull.h>
#include <nng/protocol/pipeline0/push.h>
#include <nng/supplemental/util/platform.h>

#include "ring_buf.h"
#include "prod_count.h"

#define is_full(buf) (RingBuf_num_free(buf) == 0)

#define SLEEP_MULTIPLE 100/70
#define SLEEP_MAX      1000
#define SLEEP_MIN      100

#define log_debug(fmt, ...) if(verbose >= 2) \
    { printf(fmt "\n", my_pid __VA_OPT__(,) __VA_ARGS__); }
#define log_info(fmt, ...) if(verbose >= 1) \
    { printf(fmt "\n", my_pid __VA_OPT__(,) __VA_ARGS__); }
#define log_error(fmt, ...) \
    { fprintf(stderr, fmt "\n", my_pid __VA_OPT__(,) __VA_ARGS__); }

#define fatal(func, rv) \
  { fprintf(stderr, "%d %s: %s\n", my_pid, func, nng_strerror(rv)); \
	exit(1); }

// Number of elements needed in the buffer before waking
// up the push thread.
// Note: we want this to be O(SLEEP_MIN) ms worth of receives
#define WAKE_MAX 64

enum RunState { INIT, WAIT, ACTIVE, DRAIN, DONE };

struct program_state {
    enum RunState state; // INIT,ACTIVE,DRAIN,DONE
    nng_mtx *mtx; ///< May be locked while `prod_count.mtx` is held,
                  /// but never lock `prod_count.mtx` after this.
    nng_cv *cond;
};

