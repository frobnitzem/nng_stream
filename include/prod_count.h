#ifndef _PROD_COUNT_H
#define _PROD_COUNT_H

#include <stdint.h>
#include <nng/nng.h>
#include <nng/supplemental/util/platform.h>

struct prod_count {
    nng_mtx *mtx;
    uint16_t expect; // fixed
    uint16_t seen;
    uint16_t connected;
};

int prod_count_ctor(struct prod_count *prod, const int expect);
void prod_count_dtor(struct prod_count *prod);

void prod_count_incr(struct prod_count *prod);
void prod_count_decr(struct prod_count *prod);

bool prod_count_done(struct prod_count *prod);

#endif
