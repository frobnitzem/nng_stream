#PREFIX = /sdf/home/r/rogersdd/venvs/dsn
#CFLAGS = -I$(PREFIX)/include
#LIBS += $(PREFIX)/lib64/libnng.a -lpthread
LIBS += -lnng -lpthread
CC = gcc

CFLAGS += -Iinclude

all: producer nng_cache nz_cache consumer

nng_cache: src/nng_cache.c src/ring_buf.c src/prod_count.c
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)

nz_cache: src/nz_cache.c src/ring_buf.c src/prod_count.c
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS) -lzmq

%: %.c
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)
